# Overview

# Quick start

> Warning: **Potential to lock yourself out of the target box.** One of the hardening steps configures SSH to only listen on the VPN interfaces. Make sure you have a backup method to access the server in case the VPN doesn't come up. For example, Digital Ocean provides console access through their admin panel.

> Requirement: **Currently a static IP is required** This may change in future releases with support for dynamic IP addresses. Static IPs are available on both Digital Ocean and Azure.

> Suggestion: **Use a fresh install or image as a target** This playbook should work well, but issues will be less likely and more easily resolved if you can start over easily. Also, given the potential for locking yourself out of the box, you don't want to lose access to import things that may be on an existing server.

Create a target machine using your cloud provider of choice. The CentOS 7.2, Ubuntu 16.04 and Debian 8.7 images on Digital Ocean and Microsoft's Azure have been tested and should work well. Cloud providers are ideal because you can easily spin up a test box to try things out on and delete the instance when you're done or when you no longer need the VM. Other cloud providers, a local VM or box should work fine as well but haven't been tested.

Make sure you can ssh into the target machine that will become your OpenVPN box. If using a cloud provider they should provide you with login credentials and instructions. For example, to log into the `root` account on a box with the ip `192.168.1.10` use

    ssh root@192.168.1.10

Install the required packages if you don't have them already. On Ubuntu or Debian use the commands below. On other OSes, sub-in the appropriate package manager.

    sudo apt-get install python-pip git
    sudo pip install ansible

Get *openvpn-hardened*

    git clone https://gitlab.com/JSDO1/openvpn-hardened.git

Copy the example Ansible inventory to edit for your setup. `inventory.example` has example values for different ssh configurations. If *vim* isn't your editor of choice, substitute a different editor.

    cd openvpn-hardened/
    cp inventory.example inventory
    nano inventory
    insert <server_ip>
    nano playbooks/install.yml
    insert <server_ip>

Run the install playbook

    ansible-playbook playbooks/install.yml

The playbook should run for **5-30 minutes** depending on how good your target box is at hashing and crypto operations. Assuming the above steps were successful, you should now have directory called `fetched_creds`. This contains the openvpn configuration files and private keys that can be distributed to your clients.

Try connecting to the newly created OpenVPN server

    cd fetched_creds/[server ip]/[client name]/
    openvpn [client name]@[random domain]-pki-embedded.ovpn

You'll be prompted for the private key passphrase, this is stored in a file ending in `.txt` in the client directory you just entered in the step above.


## Private key passphrases

Entering a pass phrase every time the client is started can be annoying. There are a few options to make this less burdensome after the keys have been securely distributed to the client devices.

1. When starting the client, use `openvpn --config [config] --askpass [pass.txt]` if you don't want to enter the password for the private key

2. Remove or change the passphrase on the private key

        openssl rsa -in enc.key -out not_enc.key

# Managing the OpenVPN server

## Credentials

Credentials are generated during the install process and are saved as yml formatted files in the Ansible file hierarchy so they can be used without requiring the playbook caller to take any action. The locations are below.

- CA Private key passphrase - saved in `group_vars/all.yml`
- User account name and password - saved in  `group_vars/openvpn-vpn.yml`

After the install.yml playbook has successfully been run, **you'll only be able to SSH into the box when connected to the VPN** using the account defined in `group_vars/openvpn-vpn.yml`

    ssh [created_user]@10.9.0.1

## Add clients

By default only two clients are created: `laptop` and `phone`. (These defaults can be changed by editing `openvpn_clients` in [`defaults/main.yml`](playbooks/roles/openvpn/defaults/main.yml))

Connect to the VPN before running the playbook. For example, to create a client named `cool_client` use

    ansible-playbook playbooks/add_clients.yml -e clients_to_add=cool_client

### Advanced - Adding clients using a CSR

Clients can also be added using a certificate signing request, CSR. This is useful if you intend to use keys generated and stored in a TPM. Generating the CSR will depend on your hardware, OS, TPM software, etc. If you're interested in this feature, you can probably figure this out (though [`.travis.yml`](.travis.yml) has an example of generating a CSR with *openssl*). This [blog post](https://qistoph.blogspot.nl/2015/12/tpm-authentication-in-openvpn-and-putty.html) shows how to create private key stored in a TPM and generate a CSR on Windows.

The variable `csr_path` specifies the local path to the CSR. `cn` specifies the common name specified when the CSR was created.

    ansible-playbook -e "csr_path=~/test.csr cn=test@domain.com" playbooks/add_clients.yml

This will generate the client's signed certificate and put it in `fetched_creds/[server ip]/[cn]/` as well as a nearly complete `.ovpn` client configuration file. You'll need to add references to or embed your private key and signed certificate. This will vary based on how your private key is stored. If your following the guide in the blog post mentioned above you'd do this using the OpenVPN option `cryptoapicert`.

## Revoke client access

First connect to the VPN. To revoke `cool_client`'s access

    ansible-playbook playbooks/revoke_client -e client=cool_client

## Audit server

First connect to the VPN. Run

    ansible-playbook playbooks/audit.yml

The reports will be placed in `fetched_creds/[client_ip]/`

# Contributing

Contributions via pull request, feedback, bug reports are all welcome.

# TODO

**Modify OpenVPN config to push DNS settings to stop DNS leaks**
